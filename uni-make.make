; Core version
; ------------
; Each makefile should begin by declaring the core version of Drupal that all
; projects should be compatible with.

core = '7.x'

; API version
; ------------
; Every makefile needs to declare its Drush Make API version. This version of
; drush make uses API version `2`.

api = 2

; Core project
; ------------
; In order for your makefile to generate a full Drupal site, you must include
; a core project. This is usually Drupal core, but you can also specify
; alternative core projects like Pressflow. Note that makefiles included with
; install profiles *should not* include a core project.

; Drupal 7.x. Requires the `core` property to be set to 7.x.
projects[] = drupal



; Modules
; --------
projects[admin_menu][subdir] = "contrib"
projects[module_filter][subdir] = "contrib"
projects[ctools][subdir] = "contrib"
projects[date][subdir] = "contrib"
projects[devel][subdir] = "contrib"
projects[features][subdir] = "contrib"
projects[backup_migrate][subdir] = "contrib"
projects[diff][subdir] = "contrib"
projects[field_group][subdir] = "contrib"
projects[wysiwyg][type] = "module"
projects[wysiwyg][subdir] = "contrib"
projects[views][type] = "module"
projects[views][subdir] = "contrib"
projects[views_bulk_operations][type] = "module"
projects[views_bulk_operations][subdir] = "contrib"

projects[adminimal_admin_menu][type] = "module"
projects[adminimal_admin_menu][subdir] = "contrib"

projects[backup_migrate][type] = "module"
projects[backup_migrate][subdir] = "icici"


; Entities
projects[entity][type] = "module"
projects[entity][subdir] = "contrib"
projects[eck][type] = "module"
projects[eck][subdir] = "contrib"
projects[entityreference][type] = "module"

projects[devel_themer][subdir] = "contrib"

; Themes
; --------
projects[adminimal_theme][subdir] = "contrib"

; Libraries
; ---------
;libraries[jquery][download][type] = "file"
;libraries[jquery][download][url] = "https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"


